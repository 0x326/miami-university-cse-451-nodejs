#!/usr/bin/env bash

# MovieTest
aws lambda update-function-configuration --function-name MovieTest --handler awsLambda.movieList
aws lambda update-function-code --function-name MovieTest --zip-file fileb://dist/awsLambda.zip

# MovieComments
aws lambda update-function-configuration --function-name MovieComment --handler awsLambda.movieComment
aws lambda update-function-code --function-name MovieComment --zip-file fileb://dist/awsLambda.zip

# AlexaCalculator
aws lambda update-function-configuration --function-name AlexaCalculator --handler alexaCalculator.handler
aws lambda update-function-code --function-name AlexaCalculator --zip-file fileb://dist/alexaCalculator.zip

# Robot
aws lambda update-function-configuration --function-name Robot --handler getRobot.handler
aws lambda update-function-code --function-name Robot --zip-file fileb://dist/getRobot.zip

# MoveRobot
aws lambda update-function-configuration --function-name MoveRobot --handler moveRobot.handler
aws lambda update-function-code --function-name MoveRobot --zip-file fileb://dist/moveRobot.zip
