# Move robot dialog

## Example 1

Alexa, tell john meyers robot to move Alice forward

> Ok

Alexa, tell john meyers robot to move Alice forward `{number}` steps

> Ok

Alexa, tell john meyers robot to turn Alice towards the right/left

> Ok

Alexa, ask john meyers robot where is Alice

> Alice is at position X, Y facing north. Alice can move `{number}` steps forward from this position

## Example 2

Alexa, tell john meyers robot to move Alice forward

> Sorry, but the way forward is blocked

Alexa, tell john meyers robot to move Alice forward `{number}` steps

> Sorry, but Alice can only move forward `{number}` steps
