import {
  RequestHandler,
  HandlerInput,
  SkillBuilders,
  ErrorHandler,
  getSlotValue,
} from 'ask-sdk'

import {
  DynamoDbPersistenceAdapter,
} from 'ask-sdk-dynamodb-persistence-adapter'

import {
  Response,
} from 'ask-sdk-model'

const appName = 'Calculator'

interface Session {
  runningTally: number;
}

class LaunchRequestHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'LaunchRequest'
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      attributesManager,
      requestEnvelope: {
        request,
      },
      responseBuilder,
    } = handlerInput

    if (request.type !== 'LaunchRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const requestAttributes = attributesManager.getRequestAttributes()
    // Should this be .getSessionAttributes()?
    const attributes = await attributesManager.getPersistentAttributes() || {}

    const newSessionAttributes: Session = {
      runningTally: 0,
    }

    attributesManager.setSessionAttributes(newSessionAttributes)

    return responseBuilder
      .speak(`Welcome to ${appName}. Would you like to sum up numbers?`)
      .reprompt('Would you like to sum up numbers?')
      .getResponse()
  }
}

class HelpIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.HelpIntent'
  }
  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      requestEnvelope: {
        request,
      },
      responseBuilder,
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    return responseBuilder
      .speak('I can add numbers in a running tally. What number of would like me to add next?')
      .reprompt('What number of would like me to add next?')
      .getResponse()
  }
}

class CancelAndStopIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest'
      && (request.intent.name === 'AMAZON.CancelIntent'
        || request.intent.name === 'AMAZON.StopIntent')
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope: {
        request,
      },
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const speechText = `Thank you for using ${appName}`

    return responseBuilder
      .speak(speechText)
      .withSimpleCard(appName, speechText)
      .withShouldEndSession(true)
      .getResponse()
  }
}

class SessionEndedRequestRequestHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'SessionEndedRequest'
  }
  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      requestEnvelope: {
        request,
      },
      responseBuilder,
    } = handlerInput

    if (request.type !== 'SessionEndedRequest') {
      return responseBuilder
        .getResponse()
    }
    console.log('Session ended with reason:', request.reason);
    return responseBuilder
      .getResponse()
  }
}

class IntentErrorHandler implements ErrorHandler {
  canHandle(handlerInput: HandlerInput, error: Error): boolean | Promise<boolean> {
    return true
  }
  handle(handlerInput: HandlerInput, error: Error): Response | Promise<Response> {
    const {
      responseBuilder,
    } = handlerInput

    console.error('Error handled:', error.message)

    return responseBuilder
      .speak("Sorry, I can't understand the command. Can you say it again in a different way?")
      .reprompt('Can you say it again in a different way?')
      .getResponse();
  }

}

class YesIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest' && request.intent.name === 'AMAZON.YesIntent'
  }
  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope: {
        request,
      },
      attributesManager,
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const {
      runningTally,
    } = attributesManager.getSessionAttributes() as Session

    return responseBuilder
      .speak(`Ok, the running sum is currently ${runningTally}. What would you like to add to it?`)
      .reprompt(`What would you like to add to ${runningTally}?`)
      .getResponse()
  }

}

class NoIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest' && request.intent.name === 'AMAZON.NoIntent'
  }
  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope: {
        request,
      },
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    return responseBuilder
      .speak(`Ok. If you would like to add numbers in the future, just say open ${appName}`)
      .withShouldEndSession(true)
      .getResponse()
  }

}

class AddNumberIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest' && request.intent.name === 'AddNumberIntent'
  }

  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      attributesManager,
      requestEnvelope,
      responseBuilder,
    } = handlerInput

    if (requestEnvelope.request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const {
      runningTally,
    } = attributesManager.getSessionAttributes() as Session
    const number = Number(getSlotValue(requestEnvelope, 'number'))

    const newRunningTally = runningTally + number
    const newSessionAttributes: Session = {
      runningTally: newRunningTally,
    }
    attributesManager.setSessionAttributes(newSessionAttributes)

    return responseBuilder
      .speak(`Ok, the running sum is currently ${newRunningTally}. Is there anything else you would like to add to it?`)
      .reprompt(`What else would you like to add to ${newRunningTally}?`)
      .getResponse()
  }
}

const handler = SkillBuilders.custom()
  .withPersistenceAdapter(new DynamoDbPersistenceAdapter({
    tableName: 'Calculator',
    createTable: true,
  }))
  .addRequestHandlers(
    new LaunchRequestHandler(),
    new HelpIntentHandler(),
    new CancelAndStopIntentHandler(),
    new SessionEndedRequestRequestHandler(),
    new YesIntentHandler(),
    new NoIntentHandler(),
    new AddNumberIntentHandler(),
  )
  .addErrorHandlers(
    new IntentErrorHandler(),
  )
  .lambda()

export {
  handler,
}
