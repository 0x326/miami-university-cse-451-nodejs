import AWS from 'aws-sdk'

import {
  getRobotState,
} from './robotLib'

AWS.config.update({
  region: 'us-east-1',
})

const CORSHeaders = {
  'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
  'Access-Control-Allow-Methods': 'GET,OPTIONS',
  'Access-Control-Allow-Origin': '*',
}

import {
  APIGatewayProxyHandler,
} from 'aws-lambda'

const handler: APIGatewayProxyHandler = async (event) => {
  const {
    httpMethod,
    pathParameters,
  } = event

  const {
    robot
  } = pathParameters || { robot: 'alice' }

  console.log('Got HTTP request', httpMethod, 'robot', robot)

  switch (httpMethod) {
    default:
      console.log('Sending HTTP 405', 'method', httpMethod)
      return {
        statusCode: 405,
        headers: {
          ...CORSHeaders,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(null),
      }

    case 'GET':
      try {
        const state = await getRobotState(robot)

        if (state === null) {
          console.log('Sending HTTP 400')
          return {
            statusCode: 400,
            headers: {
              ...CORSHeaders,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(null),
          }
        }

        console.log('Sending HTTP 200', 'state', state)
        return {
          statusCode: 200,
          headers: {
            ...CORSHeaders,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(state),
        }
      } catch (error) {
        console.error('Sending HTTP 500', error)
        return {
          statusCode: 500,
          headers: {
            ...CORSHeaders,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(null),
        }
      }
  }
}

export {
  handler,
}
