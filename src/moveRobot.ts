import _ from 'lodash'

import {
  RequestHandler,
  HandlerInput,
  SkillBuilders,
  ErrorHandler,
  getSlotValue,
} from 'ask-sdk'

import {
  Response,
} from 'ask-sdk-model'

import {
  init,
  getRobotState,
  moveForward,
  setRobotState,
  turnDirection,
  canMoveForward,
} from './robotLib'

const appName = 'john meyers robot'

class LaunchRequestHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'LaunchRequest'
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      attributesManager,
      requestEnvelope: {
        request,
      },
      responseBuilder,
    } = handlerInput

    if (request.type !== 'LaunchRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    await init()

    return responseBuilder
      .speak(`Welcome to ${appName}. Tell me to move a robot.`)
      .reprompt('Tell me to move a robot.')
      .getResponse()
  }
}

class HelpIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.HelpIntent'
  }
  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      requestEnvelope: {
        request,
      },
      responseBuilder,
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    return responseBuilder
      .speak(`I can help you move simulated robots. Just say tell ${appName} to move Alice forward. What would you like me to do?`)
      .reprompt('What would you like to me do?')
      .getResponse()
  }
}

class CancelAndStopIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest'
      && (request.intent.name === 'AMAZON.CancelIntent'
        || request.intent.name === 'AMAZON.StopIntent')
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope: {
        request,
      },
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const speechText = `Thank you for using ${appName}`

    return responseBuilder
      .speak(speechText)
      .withSimpleCard(appName, speechText)
      .withShouldEndSession(true)
      .getResponse()
  }
}

class SessionEndedRequestRequestHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'SessionEndedRequest'
  }
  handle(handlerInput: HandlerInput): Response | Promise<Response> {
    const {
      requestEnvelope: {
        request,
      },
      responseBuilder,
    } = handlerInput

    if (request.type !== 'SessionEndedRequest') {
      return responseBuilder
        .getResponse()
    }
    console.log('Session ended with reason:', request.reason)
    return responseBuilder
      .getResponse()
  }
}

class IntentErrorHandler implements ErrorHandler {
  canHandle(handlerInput: HandlerInput, error: Error): boolean | Promise<boolean> {
    return true
  }
  handle(handlerInput: HandlerInput, error: Error): Response | Promise<Response> {
    const {
      responseBuilder,
    } = handlerInput

    console.error('Error handled:', error.message)

    return responseBuilder
      .speak("Sorry, but it seems an error occured when I tried to think about that. Can you try telling me it again, perhaps in a different way?")
      .reprompt('Can you say your command again, perhaps in a different way?')
      .getResponse()
  }

}

class MoveRobotIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest' && request.intent.name === 'MoveRobotIntent'
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope,
      requestEnvelope: {
        request,
      },
      attributesManager,
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const robot = getSlotValue(requestEnvelope, 'robot').toLowerCase() || 'alice'
    const steps = (() => {
      const steps = getSlotValue(requestEnvelope, 'number')
      console.log('Raw step value', steps)

      if (steps === '' || steps === null) {
        return null
      }
      return Number(steps)
    })()
    console.log('Handling MoveRobotIntent with', 'robot', robot, 'steps', steps)

    const state = await getRobotState(robot)
    console.log('Robot is currently at state', state)

    if (state === null) {
      return responseBuilder
        .speak(`I don't think I heard the robot name correctly. A robot named ${robot} does not exist. Can you try giving me the command again?`)
        .reprompt("I didn't hear you correctly that last time. Can you try saying it to me again?")
        .getResponse()
    }

    const {
      location,
      direction,
    } = state

    const otherRobotState = await (() => {
      if (robot === 'alice') {
        return getRobotState('bob')
      } else if (robot === 'bob') {
        return getRobotState('alice')
      } else {
        return null
      }
    })()

    if (otherRobotState === null) {
      console.error('Logically inconsistent state has been reached')
      return responseBuilder
        .speak("Sorry, but I can into an internal error. Please try again")
        .getResponse()
    }

    const {
      location: otherLocation,
    } = otherRobotState
    console.log('Other robot is currently at location', otherLocation)

    if (canMoveForward(location, direction, otherLocation, steps || 1) === true) {
      // Move robot
      const newLocation = moveForward(location, direction, steps || 1)
      await setRobotState(robot, {
        name: robot,
        location: newLocation,
        direction,
      })

      return responseBuilder
        .speak('Ok')
        .getResponse()
    }

    if (steps === null) {
      return responseBuilder
        .speak('Sorry, but something is in the way')
        .getResponse()
    }

    return responseBuilder
      .speak(`Sorry, but ${robot} can't move ${steps} spaces forward`)
      .getResponse()
  }
}

class TurnRobotIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest' && request.intent.name === 'TurnRobotIntent'
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope,
      requestEnvelope: {
        request,
      },
      attributesManager,
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const robot = getSlotValue(requestEnvelope, 'robot').toLowerCase() || 'alice'
    const rotationDirection = (() => {
      const direction = getSlotValue(requestEnvelope, 'direction').toLowerCase()

      switch (direction) {
        default:
          return null

        case 'left':
        case 'ccw':
          return 'CCW'

        case 'right':
        case 'cw':
          return 'CW'
      }
    })()
    console.log('Handling TurnRobotIntent with', 'robot', robot, 'rotationDirection', rotationDirection)

    const state = await getRobotState(robot)

    if (state === null) {
      return responseBuilder
        .speak(`I don't think I heard the robot name correctly. A robot named ${robot} does not exist. Can you try giving me the command again?`)
        .reprompt("I didn't hear you correctly that last time. Can you try saying it to me again?")
        .getResponse()
    }

    if (rotationDirection === null) {
      return responseBuilder
        .speak(`I didn't hear which direction I should turn the ${robot} robot. Can you try giving me the full command again?`)
        .reprompt("I didn't hear you correctly that last time. Can you try saying it to me again?")
        .getResponse()
    }

    const {
      location,
      direction,
    } = state

    const newDirection = turnDirection(direction, rotationDirection)

    await setRobotState(robot, {
      name: robot,
      location,
      direction: newDirection,
    })

    return responseBuilder
      .speak('Ok')
      .getResponse()
  }
}

class GetRobotLocationIntentHandler implements RequestHandler {
  canHandle(handlerInput: HandlerInput): boolean | Promise<boolean> {
    const {
      requestEnvelope: {
        request,
      },
    } = handlerInput

    return request.type === 'IntentRequest' && request.intent.name === 'GetRobotLocationIntent'
  }
  async handle(handlerInput: HandlerInput): Promise<Response> {
    const {
      responseBuilder,
      requestEnvelope,
      requestEnvelope: {
        request,
      },
      attributesManager,
    } = handlerInput

    if (request.type !== 'IntentRequest') {
      console.error("I am handling a request I don't want to handle")
      return responseBuilder
        .getResponse()
    }

    const robot = getSlotValue(requestEnvelope, 'robot').toLowerCase() || 'alice'
    console.log('Handling GetRobotLocationIntent with', 'robot', robot)

    const state = await getRobotState(robot)

    if (state === null) {
      return responseBuilder
        .speak(`I don't think I heard the robot name correctly. A robot named ${robot} does not exist. Can you try giving me the command again?`)
        .reprompt("I didn't hear you correctly that last time. Can you try saying it to me again?")
        .getResponse()
    }

    const {
      location: [x, y],
    } = state

    return responseBuilder
      .speak(`${robot} is at coordinate ${x} ${y}.`)
      .getResponse()
  }
}

const handler = SkillBuilders.custom()
  .addRequestHandlers(
    new LaunchRequestHandler(),
    new HelpIntentHandler(),
    new CancelAndStopIntentHandler(),
    new SessionEndedRequestRequestHandler(),
    new MoveRobotIntentHandler(),
    new TurnRobotIntentHandler(),
    new GetRobotLocationIntentHandler(),
  )
  .addErrorHandlers(
    new IntentErrorHandler(),
  )
  .lambda()

export {
  handler,
}
