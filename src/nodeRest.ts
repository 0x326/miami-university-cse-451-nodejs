import http from 'http'

import AWS from 'aws-sdk'

AWS.config.update({
  region: 'us-east-1',
})

const s3Client = new AWS.S3({
  apiVersion: '2006-03-01',
})

const dynamoDBClient = new AWS.DynamoDB.DocumentClient()

http
  .createServer(async (request, response) => {
    const { url } = request
    if (url === undefined) {
      return response
        .writeHead(400, {
          'Content-Type': 'application/json',
        })
        .end(JSON.stringify(null))
    }

    console.log('Got request for URL', url)
    const urlParts = url.split('/')

    switch (urlParts[1]) {
      default:
        console.log('Sending HTTP 404')
        response
          .writeHead(404, {
            'Content-Type': 'application/json',
          })
          .end(JSON.stringify(null))
        break;

      case 'v1':
        switch (urlParts[2]) {
          default:
            console.log('Sending HTTP 404')
            response
              .writeHead(404, {
                'Content-Type': 'application/json',
              })
              .end(JSON.stringify(null))
            break;

          case 's3':
            try {
              const { Buckets: buckets } = await s3Client.listBuckets().promise()
              if (buckets !== undefined) {
                console.log('Sending buckets', buckets)
                response
                  .writeHead(200, {
                    'Content-Type': 'application/json',
                  })
                  .end(JSON.stringify(buckets))
              } else {
                console.error('Sending HTTP 500')
                response
                  .writeHead(500, {
                    'Content-Type': 'application/json',
                  })
                  .end(JSON.stringify([]))
              }
            } catch (error) {
              console.error('Sending HTTP 500', error)
              response
                .writeHead(500, {
                  'Content-Type': 'application/json',
                })
                .end(JSON.stringify([]))
            }
            break;

          case 'dynamo':
            const year = Number(urlParts[3])
            if (isNaN(year)) {
              console.log('Sending HTTP 400')
              response
                .writeHead(400, {
                  'Content-Type': 'application/json',
                })
                .end(JSON.stringify([]))
            } else {
              const { Items: items } = await dynamoDBClient
                .query({
                  TableName: 'Movies',
                  KeyConditionExpression: '#yr = :yyyy',
                  ExpressionAttributeNames: {
                    '#yr': 'year',
                  },
                  ExpressionAttributeValues: {
                    ':yyyy': year,
                  },
                })
                .promise()

              if (items !== undefined) {
                console.log('Sending items', items)
                response
                  .writeHead(200, {
                    'Content-Type': 'application/json',
                  })
                  .end(JSON.stringify(items))
              } else {
                console.log('Sending HTTP 500')
                response
                  .writeHead(500, {
                    'Content-Type': 'application/json',
                  })
                  .end(JSON.stringify([]))
              }
            }
            break;
        }
    }
  })
  .listen(8083)
