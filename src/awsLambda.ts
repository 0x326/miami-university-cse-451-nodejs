import AWS from 'aws-sdk'

import {
  APIGatewayProxyHandler,
} from 'aws-lambda'

AWS.config.update({
  region: 'us-east-1',
})

const dynamoDBClient = new AWS.DynamoDB.DocumentClient()

const CORSHeaders = {
  'Access-Control-Allow-Headers': 'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
  'Access-Control-Allow-Methods': 'GET,OPTIONS',
  'Access-Control-Allow-Origin': '*',
}

const movieList: APIGatewayProxyHandler = async (event) => {
  const {
    httpMethod,
    body,
    pathParameters,
  } = event

  const {
    year
  } = pathParameters || { year: '1970' }

  const parsedYear = parseInt(year)

  console.log('Got HTTP request', httpMethod, 'year', parsedYear)

  switch (httpMethod) {
    default:
      console.log('Sending HTTP 405', 'method', httpMethod)
      return {
        statusCode: 405,
        headers: {
          ...CORSHeaders,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(null),
      }

    case 'GET':
      try {
        if (isNaN(parsedYear)) {
          console.log('Sending HTTP 400', 'year', year)
          return {
            statusCode: 400,
            headers: {
              ...CORSHeaders,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify([]),
          }
        }

        const { Items: items } = await dynamoDBClient
          .query({
            TableName: 'Movies',
            KeyConditionExpression: '#yr = :yyyy',
            ExpressionAttributeNames: {
              '#yr': 'year',
            },
            ExpressionAttributeValues: {
              ':yyyy': parsedYear,
            },
          })
          .promise()

        if (items !== undefined) {
          console.log('Sending HTTP 200', 'items', items)
          return {
            statusCode: 200,
            headers: {
              ...CORSHeaders,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(items),
          }
        } else {
          console.error('Sending HTTP 500', 'year', year)
          return {
            statusCode: 500,
            headers: {
              ...CORSHeaders,
              'Content-Type': 'application/json',
            },
            body: JSON.stringify([]),
          }
        }
      } catch (error) {
        console.error('Sending HTTP 500', error)
        return {
          statusCode: 500,
          headers: {
            ...CORSHeaders,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify([]),
        }
      }
  }
}

interface MovieComment {
  title: string;
  year: number;
  commentText: string;
  commentAuthor: string;
}

const movieComment: APIGatewayProxyHandler = async (event) => {
  const {
    httpMethod,
    body,
  } = event

  let parsedBody: MovieComment | null = null
  try {
    parsedBody = JSON.parse(body || 'null')
  } catch (error) {
    console.error('Got HTTP request with bad JSON', 'method', httpMethod, 'body', body)
  }
  console.log('Got HTTP request', httpMethod, 'body', parsedBody)

  if (parsedBody === null) {
    return {
      statusCode: 400,
      headers: {
        ...CORSHeaders,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(null),
    }
  }

  switch (httpMethod) {
    default:
      console.log('Sending HTTP 405', 'method', httpMethod)
      return {
        statusCode: 405,
        headers: {
          ...CORSHeaders,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(null),
      }

    case 'PUT':
      const {
        title,
        year,
        commentText,
        commentAuthor,
      } = parsedBody

      console.log('Getting comment for', title, year)
      const { Item: item } = await dynamoDBClient
        .get({
          TableName: 'Movies',
          Key: {
            year,
            title,
          }
        })
        .promise()

      if (item === undefined) {
        return {
          statusCode: 400,
          headers: {
            ...CORSHeaders,
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(null),
        }
      }

      const comment = {
        text: commentText,
        author: commentAuthor,
      }

      console.log('Updating comment for', title, year)
      await dynamoDBClient
        .update({
          TableName: 'Movies',
          Key: {
            year,
            title,
          },
          UpdateExpression: 'set movieComment = :c',
          ExpressionAttributeValues: {
            ':c': comment,
          },
          ReturnValues: 'NONE',
        })
        .promise()

      console.log('Sending HTTP 200')
      return {
        statusCode: 200,
        headers: {
          ...CORSHeaders,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(null)
      }
  }
}

export {
  movieList,
  movieComment,
}
