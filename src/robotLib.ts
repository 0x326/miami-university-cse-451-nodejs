import _ from 'lodash'
import AWS from 'aws-sdk'

AWS.config.update({
  region: 'us-east-1',
})

type North = [0, -1]
type South = [0, 1]
type East = [1, 0]
type West = [-1, 0]
type Direction = North | South | East | West
type Rotation = 'CW' | 'CCW'

type RobotName = string
type RobotLocation = [number, number]
interface Robot {
  name: RobotName;
  location: RobotLocation;
  direction: Direction;
}

const gridWidth = 200
const gridHeight = 200

const obstacleMap = new Map<string, boolean>()
for (const x of _.range(gridWidth)) {
  for (const y of _.range(gridHeight)) {
    const isObstacle = x == 0 || y == 0 || x == gridWidth - 1 || y == gridHeight - 1
    obstacleMap.set(`${x}-${y}`, isObstacle)
  }
}

const dynamoDBClient = new AWS.DynamoDB()
const dynamoDBDocumentClient = new AWS.DynamoDB.DocumentClient()

async function init() {
  try {
    await dynamoDBClient
      .createTable({
        TableName: 'Robot',
        KeySchema: [
          { AttributeName: 'name', KeyType: 'HASH' },  // Partition key
        ],
        AttributeDefinitions: [
          { AttributeName: 'name', AttributeType: 'S' },
        ],
        ProvisionedThroughput: {
          ReadCapacityUnits: 10,
          WriteCapacityUnits: 10
        }
      })
      .promise()
  } catch (error) {
    // Do nothing
  }

  await dynamoDBDocumentClient
    .update({
      TableName: 'Robot',
      Key: {
        name: 'alice',
      },
      UpdateExpression: 'set #l = :l, #d = :d',
      ExpressionAttributeNames: {
        '#l': 'location',
        '#d': 'direction',
      },
      ExpressionAttributeValues: {
        ':l': [5, 5],
        ':d': [1, 0],
      },
      ReturnValues: 'NONE',
    })
    .promise()

  await dynamoDBDocumentClient
    .update({
      TableName: 'Robot',
      Key: {
        name: 'bob',
      },
      UpdateExpression: 'set #l = :l, #d = :d',
      ExpressionAttributeNames: {
        '#l': 'location',
        '#d': 'direction',
      },
      ExpressionAttributeValues: {
        ':l': [10, 10],
        ':d': [1, 0],
      },
      ReturnValues: 'NONE',
    })
    .promise()
}

async function getRobotNames(): Promise<Array<RobotName>> {
  console.log('Getting robot names')
  const { Items: items } = await dynamoDBDocumentClient
    .query({
      TableName: 'Robot',
    })
    .promise()

  if (items === undefined) {
    throw new Error('DynamoDB Error while getting robot names')
  }
  const robots = items as Robot[]

  const robotNames = robots.map((item) => item.name)
  console.log('Got robot names', robotNames)
  return robotNames
}

async function getRobotState(name: RobotName): Promise<Robot | null> {
  console.log('Getting robot state for name', name)
  const { Items: items } = await dynamoDBDocumentClient
    .query({
      TableName: 'Robot',
      KeyConditionExpression: '#n = :n',
      ExpressionAttributeNames: {
        '#n': 'name',
      },
      ExpressionAttributeValues: {
        ':n': name,
      },
    })
    .promise()

  if (items === undefined) {
    console.log('Could not get robot state')
    return null
  }
  const [robot] = items as Robot[]
  if (robot === undefined) {
    console.log('Could not get robot state')
    return null
  }

  console.log('Got robot state', robot)
  return robot
}

async function setRobotState(robot: RobotName, state: Robot): Promise<void> {
  console.log('Setting robot', robot, 'state', state)
  const {
    location,
    direction,
  } = state

  await dynamoDBDocumentClient
    .update({
      TableName: 'Robot',
      Key: {
        name: robot,
      },
      UpdateExpression: 'set #l = :l, #d = :d',
      ExpressionAttributeNames: {
        '#l': 'location',
        '#d': 'direction',
      },
      ExpressionAttributeValues: {
        ':l': location,
        ':d': direction,
      },
      ReturnValues: 'NONE',
    })
    .promise()
  console.log('Set robot state')
}

function moveForward(location: RobotLocation, direction: Direction, steps = 1): RobotLocation {
  const [x, y] = location
  const [dx, dy] = direction

  return [x + steps * dx, y + steps * dy]
}

function turnDirection(direction: Direction, rotationDirection: Rotation): Direction {
  const [dx, dy] = direction

  if (rotationDirection == 'CW') {
    return [-dy, dx] as Direction
  } else {
    return [dy, -dx] as Direction
  }
}

function canMoveForward(robotLocation: RobotLocation, direction: Direction, otherRobotLocation: RobotLocation, steps = 1): boolean {
  const newLocation = moveForward(robotLocation, direction)
  console.log('Checking move from', robotLocation, 'to', newLocation, 'in direction', direction, 'with', steps - 1, 'more steps to go')
  const [newX, newY] = newLocation

  if (obstacleMap.get(`${newX}-${newY}`) === false) {
    if (newLocation !== otherRobotLocation) {
      if (steps <= 1) {
        console.log('Robot can move forward')
        return true
      } else {
        return canMoveForward(newLocation, direction, otherRobotLocation, steps - 1)
      }
    } else {
      console.log('Robot is in the way')
      return false
    }
  } else if (obstacleMap.get(`${newX}-${newY}`) === true) {
    console.log('Obstacle is in the way')
    return false
  } else {
    throw new Error(`Location ${newLocation} not found`)
  }
}

export {
  obstacleMap,
  init,
  getRobotNames,
  getRobotState,
  setRobotState,
  moveForward,
  turnDirection,
  canMoveForward,
}
