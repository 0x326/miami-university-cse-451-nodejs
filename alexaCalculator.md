# Calculator dialog

## Example 1

Alexa, open calculator

> Welcome to Calculator.
> Would you like to add numbers?

Yes

> Ok, the running sum is currently zero.
> What would you like to add to it?

Ten

> Ok, the running sum is currently ten.
> Is there anything else you would like to add to it?

Seven

> Ok, the running sum is currently seventeen.
> Is there anything else you would like to add to it?

Stop/nope

> Thanks for using calculator

## Example 2

Alexa, open calculator

> Welcome to Calculator.
> Would you like to add numbers?

No

> Ok.
> If you like to add number in the future, just say "open calculator".
